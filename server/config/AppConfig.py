import os
from redis import Redis


class Config(object):
    # 调试模式是否开启
    DEBUG = True
    ENV = "development"
    # ENV = "production"
    FLASK_DEBUG = 2

    SEND_FILE_MAX_AGE_DEFAULT = 1

    # 如果设置成 True (默认情况)，Flask-SQLAlchemy 将会追踪对象的修改并且发送信号。这需要额外的内存， 如果不必要的可以禁用它。
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # SECRET_KEY=os.urandom(24)
    # SESSION_TYPE = 'redis'  # session存储格式为redis
    # SESSION_REDIS = Redis(host='11.11.11.14', port=6379, password='armylong')  # redis的服务器参数
    SESSION_USE_SIGNER = True  # 是否强制加盐，混淆session
    SECRET_KEY = os.urandom(24)  # 上面开启了强制加盐,这边要设置盐
    SESSION_PERMANENT = True  # sessons是否长期有效，True:31天过期,False:关闭浏览器session就失效。
    PERMANENT_SESSION_LIFETIME = 86400 * 30  # session长期有效，则设定session生命周期，整数秒，默认大概不到3小时。

    # mysql数据库连接信息
    # SQLALCHEMY_DATABASE_URI = "mysql+pymysql://armylong:armylong@11.11.11.13:6033/armylong?charset=utf8mb4"
