# 数据库字段统一定义
class DatabasesFieldUnifyDefined(object):
    # status字段统一定义:1打开/2关闭/3删除
    STATUS_RANGE = [0, 1, 99]
    STATUS_OPEN = 1
    STATUS_CLOSE = 0
    STATUS_DEL = 99


# static.armylong.com静态文件目录定义
class StaticPath(object):
    # http://static.armylong.com
    BASE_URL = "http://static.armylong.com/"

    # /root/FileBox/
    BASE_PATH = "/www/static.armylong.com/"

    # img/
    IMG = "img/"
    IMG_PATH = BASE_PATH + IMG
    IMG_WEB = BASE_URL + IMG

    # img/WebStatic/
    WEB_STATIC = "WebStatic/"
    WEB_STATIC_PATH = IMG_PATH + WEB_STATIC
    WEB_STATIC_WEB = IMG_WEB + WEB_STATIC

    # img/Avatar/
    AVATAR = "Avatar/"
    AVATAR_PATH = IMG_PATH + AVATAR
    AVATAR_WEB = IMG_WEB + AVATAR

    # img/sevenDrangon/
    SEVEN_DRANGON = "SevenDrangon/"
    SEVEN_DRANGON_PATH = IMG_PATH + SEVEN_DRANGON
    SEVEN_DRANGON_WEB = IMG_WEB + SEVEN_DRANGON

    # img/CloudText/
    CLOUD_TEXT = "CloudText/"
    CLOUD_TEXT_PATH = IMG_PATH + CLOUD_TEXT
    CLOUD_TEXT_WEB = IMG_WEB + CLOUD_TEXT

    # img/ResizeImg/
    RESIZE_IMG = "ResizeImg/"
    RESIZE_IMG_PATH = IMG_PATH + RESIZE_IMG
    RESIZE_IMG_WEB = IMG_WEB + RESIZE_IMG

    # img/DingDing/
    DINGDING_SCREENSHOT = "DingDing/"
    DINGDING_SCREENSHOT_PATH = IMG_PATH + DINGDING_SCREENSHOT
    DINGDING_SCREENSHOT_WEB = IMG_WEB + DINGDING_SCREENSHOT

    # dingding
    DING_DING = "dingding/"
    DING_DING_PATH = BASE_PATH + DING_DING
    DING_DING_WEB = BASE_URL + DING_DING

    # xls_json/
    XLS_JSON = "xls_json/"
    XLS_JSON_PATH = BASE_PATH + XLS_JSON
    XLS_JSON_WEB = BASE_URL + XLS_JSON

    # json_xls/
    JSON_XLS = "json_xls/"
    JSON_XLS_PATH = BASE_PATH + JSON_XLS
    JSON_XLS_WEB = BASE_URL + JSON_XLS


class DingDing(object):
    DINGDING = {
        "user": {"fileName": "dingding-user", "init": "armylong"},
        "card": {"fileName": "dingding-card", "init": "0"}
    }
    USER_LIST = ["armylong"]
    CARD_LIST = ['1', '2', '3', '4']

    CLASSINFO = {
        "0": {"info": "已停止"},
        "1": {"info": "仅登陆"},
        "2": {"info": "上班打卡"},
        "3": {"info": "下班打卡"},
        "4": {"info": "下班更新"},
    }


if __name__ == '__main__':
    dbStatus = DatabasesFieldUnifyDefined.__dict__
    print(dbStatus)
    for k, v in dbStatus.items():
        print(k, v)
    print("\n")

    static = StaticPath.__dict__
    print(static)
    for k, v in static.items():
        print(k, v)
