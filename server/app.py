import util.InitProject

import base64
from flask import request, session, g
from application import app
from util.ToJson import ToJson


@app.route('/api/', methods=['GET', 'POST'])
def hello_flask():
    params = request.values
    g.params = request.values
    return welcome(params)


@app.route('/api/access/', methods=['GET', 'POST'])
def _access():
    logged_in = session.get('logged_in')
    if not logged_in:
        return ToJson().code302(msg="logged_in False", data={"Location": "/"}), 302
    else:
        return ToJson().code200(msg="logged_in True")


@app.route('/api/<route_params>/', methods=['GET', 'POST'])
def welcome(route_params):
    if g.__dict__:
        params = g.params
    else:
        params = request.values
    if 'Authorization' in request.headers.items():
        authDe = base64.b64decode(request.headers['Authorization']).decode('utf-8')
    else:
        authDe = ''
    data = {
        'methods': request.method,
        'url': request.url,
        'base_url': request.base_url,
        'host': request.host,
        'host_url': request.host_url,
        'path': request.path,
        'full_path': request.full_path,
        'remote_addr': request.remote_addr,
        'params': params,
        'cookies': request.cookies,
        'Authorization': authDe,
        'content_length': request.content_length,
        'headers': dict(request.headers.items())
    }
    return ToJson.code200(data)


if __name__ == '__main__':
    app.run(host="localhost", port=2021)
    # socketio.run(app, debug=True)
    # 123
