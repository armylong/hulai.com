from flask import Flask
from flask_session import Session
from config.AppConfig import Config
from flask_sqlalchemy import SQLAlchemy


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    Session(app)

    return app


app = create_app()
# db = SQLAlchemy(app)

# from application.User import user
#
# app.register_blueprint(user, url_prefix='/api/user/')
