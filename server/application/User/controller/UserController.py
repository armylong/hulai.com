from flask import request, session
from util.ToJson import ToJson
from application.User.business.RegisterBusiness import RegisterBusiness
from application.User.business.LoginBusiness import LoginBusiness
from application.User.model.UserModel import UserModel
from application.User.model.FansModel import FansModel
from application.windows.model.AccessModel import AccessModel
import math

from .. import user


@user.before_request
def _check():
    logged_in = session.get('logged_in')

    route_pass = [
        "/api/user/register/",
        "/api/user/register",
        "/api/user/login/",
        "/api/user/login",
        "/api/user/logout/",
        "/api/user/logout",
    ]

    # 如果没登陆并且路由没在授权列表中，就跳转到首页
    if not logged_in and request.path not in route_pass:
        return ToJson.code302(msg="logged_in False", data={"Location": "/"}), 302


@user.route('/register/', methods=['POST'])
def _register():
    username = request.values.get("username")
    password = request.values.get("password")
    repassword = request.values.get("repassword")

    reg = RegisterBusiness(username=username, password=password, repassword=repassword)
    # 检查参数
    checkParams = reg.checkParams()
    if not checkParams:
        return reg.errCode

    addUser = reg.addUser()
    # 添加用户
    if not addUser:
        return reg.errCode

    # 注册成功后调用登陆的储存cookie方法
    login = LoginBusiness(username=username, password=password)
    login.saveCookie()

    # redirect负责重新定向,接收2个参数,第一个是URL地址,第二个参数为code状态码,
    # 跳转(301)多用于旧网址在废弃前转向新网址以保证用户的访问,表示永久重定向,
    # 而重定向(302)表示页面暂时性的转移,flask.redirect默认就是302零时重定向
    # return redirect("http://api.armylong.com")
    return ToJson.code200(data={"Location": "/"}), 302
    # return ToJson.code200()


@user.route('/login/', methods=['GET', 'POST'])
def _login():
    username = request.values.get("username")
    password = request.values.get("password")

    login = LoginBusiness(username=username, password=password)

    if not login.checkParams():
        return login.errCode

    if not login.checkUser():
        return login.errCode

    login.saveCookie()

    return ToJson.code200(msg="zzl1122", data={"Location": "/"}), 302


@user.route('/logout/', methods=['GET', 'POST'])
def _logout():
    session.clear()
    return ToJson.code200()


@user.route('/list/', methods=['GET', 'POST'])
def _list():
    page = request.values.get("page")
    if not page:
        page = 1
    else:
        page = int(page)
    limit = request.values.get("limit")
    if not limit:
        limit = 20
    else:
        limit = int(limit)

    userList = UserModel().userList(page=page, limit=limit)
    allPage = math.ceil(len(userList) / limit)

    for i in range(len(userList)):
        userList[i]['accessCN'] = AccessModel().getAccessCnList(userList[i]['access'])

    resData = {"allPage": allPage, "userList": userList}

    return ToJson.code200(data=resData)
