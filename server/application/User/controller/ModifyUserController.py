import os

from flask import request
from flask import session

from util.ToJson import ToJson
from util.PasswordMD5 import PasswordMD5
from application import db
from application.User.model.UserModel import UserModel

from util.CompressImage import CompressImage
from config.VarGlobal import StaticPath

from application.User import user


@user.route(rule="/nickname/", methods=["GET", "POST"])
def _nickname():
    nickname = request.values.get("nickname")
    if not nickname:
        return ToJson.code500(msg="没有输入新的昵称")
    nickname = nickname.replace(" ", "")
    if len(nickname) > 10:
        return ToJson.code500(msg="昵称不能大于十位")

    uid = session.get('uid')
    user = UserModel.query.get(uid)
    user.nickname = nickname
    db.session.commit()
    db.session.close()

    return ToJson.code200()


@user.route(rule="/mobile/", methods=["GET", "POST"])
def _mobile():
    mobile = request.values.get("mobile")
    if not mobile:
        return ToJson.code500(msg="没有输入新手机号")
    mobile = mobile.replace(" ", "")
    try:
        int(mobile)
    except:
        return ToJson.code500(msg="手机号不合法")
    if len(mobile) != 11:
        return ToJson.code500(msg="手机号不合法")
    if mobile[0] != "1":
        return ToJson.code500(msg="手机号不合法")

    uid = session.get('uid')
    user = UserModel.query.get(uid)
    user.mobile = mobile
    db.session.commit()
    db.session.close()

    return ToJson.code200()


@user.route(rule="/password/", methods=["GET", "POST"])
def _password():
    password_old = request.values.get("password_old")
    password_new = request.values.get("password_new")

    if not password_old:
        return ToJson.code500(msg="请输入旧密码")
    if not password_new:
        return ToJson.code500(msg="请输入新密码")
    if " " in password_new:
        return ToJson.code500(msg="密码中不能有空格")
    if len(password_new) < 4 or len(password_new) > 20:
        return ToJson().code500(msg="密码需要最小4位,最大20位")

    uid = session.get('uid')
    user = UserModel.query.get(uid)
    username = user.username

    password_old_md5 = PasswordMD5().password_MD5(password=password_old, salt_private=username)
    password_old_check = UserModel.query.filter(UserModel.username == username,
                                                UserModel.password == password_old_md5).all()
    if not password_old_check:
        return ToJson.code500(msg="原密码校验未通过")

    password_new_md5 = PasswordMD5().password_MD5(password=password_new, salt_private=username)
    user.password = password_new_md5
    db.session.commit()
    db.session.close()

    return ToJson.code200()


@user.route(rule="/avatar/", methods=["GET", "POST"])
def _avatar():
    f = request.files['avatar']
    if not f:
        return ToJson.code500(msg="请上传头像")

    # 允许的文件类型
    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'JPG', 'PNG', 'bmp'])

    # 文件名必须有小数点.
    f_name = f.filename
    if "." not in f_name:
        return ToJson.code500(msg="上传的文件不合法")

    # 拆分并分析文件类型
    f_name_break = f.filename.split(".")
    f_type = f_name_break[len(f_name_break) - 1]

    if f_type not in ALLOWED_EXTENSIONS:
        return ToJson.code500(msg="图片类型不合法")

    # 图片名称更换为username.f_type
    uid = session.get('uid')
    user = UserModel.query.get(uid)
    avatar_name = user.username + "." + f_type
    avatar_path = StaticPath.AVATAR_PATH + avatar_name
    avatar_url = StaticPath.AVATAR_WEB + avatar_name

    # 把web目录存到数据库
    user.avatar = avatar_url
    db.session.commit()
    db.session.close()

    # 把文件存到web目录下
    f.save(avatar_path)

    # 裁剪并保存图片
    comImg = CompressImage(infile=avatar_path)
    comImg.autoresizeImg(toPx=238)

    # return ToJson.code200()
    return ToJson.dataBox(data={"avatar": str(avatar_url)})
