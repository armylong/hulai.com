import time

from util.ToJson import ToJson
from application.User.model.UserModel import UserModel
from application import db
from util.PasswordMD5 import PasswordMD5


class RegisterBusiness(object):
    errCode = None

    def __init__(self, username, password, repassword):
        self.username = username
        self.password = password
        self.repassword = repassword

    def checkParams(self):
        username = self.username
        password = self.password
        repassword = self.repassword
        if not username:
            self.errCode = ToJson.code500(msg="用户名不能为空")
            return False
        if not password:
            self.errCode = ToJson.code500(msg="密码不能为空")
            return False

        if password != repassword:
            self.errCode = ToJson.code500(msg="两次输入的密码不一致")
            return False

        if " " in username:
            self.errCode = ToJson.code500(msg="用户名中不能含有空格")
            return False
        if " " in password:
            self.errCode = ToJson.code500(msg="密码中不能含有空格")
            return False

        userCheck = UserModel.query.filter_by(username=username).first()
        if userCheck:
            self.errCode = ToJson.code500(msg="用户名已经被占用了")
            return False

        if len(username) > 20:
            self.errCode = ToJson.code500(msg="用户名最大20位")
            return False
        if len(password) < 4 or len(password) > 20:
            self.errCode = ToJson.code500(msg="密码需要最小4位,最大20位")
            return False

        return True

    def addUser(self):
        username = self.username
        password = PasswordMD5().password_MD5(password=self.password, salt_private=username)
        nickname = "阿米家族" + str(time.time())[0:9][::-1]
        try:
            user = UserModel(username=username, password=password, nickname=nickname)
            db.session.add(user)
            db.session.commit()
            return True
        except:
            self.errCode = ToJson.code500()
            return False
