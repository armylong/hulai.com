from util.ToJson import ToJson
from util.PasswordMD5 import PasswordMD5
from application.User.model.UserModel import UserModel
from flask import session, make_response


class LoginBusiness(object):
    errCode = None

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def checkParams(self):
        username = self.username
        password = self.password

        if not username:
            self.errCode = ToJson().code500(msg="用户名不能为空")
            return False
        if not password:
            self.errCode = ToJson().code500(msg="密码不能为空")
            return False

        return True

    def checkUser(self):
        # 因为登录账号有可能是username或mobile,所以要根据username或者mobile查找用户
        userUsername = UserModel.query.filter(UserModel.username == self.username).first()
        if not userUsername:
            userMobile = UserModel.query.filter(UserModel.mobile == self.username).first()
            if userMobile:
                self.username = userMobile.username
            else:
                # 如果都没有查到则不允许登录
                self.errCode = ToJson().code500(msg="登录失败: 用户名或密码错误")
                return False

        # 如果查到用户名,则比对密码
        password = PasswordMD5().password_MD5(password=self.password, salt_private=self.username)
        userCheck = UserModel.query.filter_by(username=self.username, password=password).first()
        if userCheck:
            return True
        else:
            self.errCode = ToJson().code500(msg="登录失败: 用户名或密码错误")
            return False

    def saveCookie(self):
        username = self.username
        user = UserModel.query.filter_by(username=username).first()
        # 登录成功,设置session: id=XXX,logged_in:True , 然后把sessin设置resp的cookie
        resp = make_response()
        session['uid'] = user.uid
        session['logged_in'] = True
        session['access'] = user.access

        # logged_in = session.get('logged_in')
        # current_app.save_session(session, resp)
        # resp.set_cookie("session111", str(session), expires="2019-06-23 22:57:58.462260")
        return True
