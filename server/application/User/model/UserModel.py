from sqlalchemy.sql import func
from sqlalchemy import TIMESTAMP
from application import db
import json
from flask import session


class UserModel(db.Model):
    __tablename__ = 'user'
    # String, Integer, Text, Boolean, SmallInteger, DateTime
    # 主键primary_key=True, 自增autoincrement=True， 是否可以为空nullable=False, 索引index=True, 注释comment='用户id', 默认server_default='0'
    # 自动更新onupdate=True
    uid = db.Column(db.SmallInteger, primary_key=True, autoincrement=True, nullable=False, index=True, comment='用户id')
    username = db.Column(db.String(20), unique=True, nullable=False, index=True, comment='用户名')
    password = db.Column(db.String(50), nullable=False, comment='密码')
    mobile = db.Column(db.String(20), nullable=False, server_default='', index=True, comment='手机号')
    nickname = db.Column(db.String(20), nullable=False, server_default='', index=True, comment='昵称')
    avatar = db.Column(db.String(50), nullable=False, server_default='', comment='用户头像')
    status = db.Column(db.SmallInteger, nullable=False, server_default='1', index=True, comment='状态{0:关闭,1:正常}')
    access = db.Column(db.String(20), nullable=False, server_default='[3]', index=True,
                       comment='用户权限{1:超级管理员,2:管理员,3:普通用户,4:学习小组,5:新氧,<99:游客>}')
    create_time = db.Column(db.DateTime, nullable=False, server_default=func.now(), index=True, comment="创建时间")
    update_time = db.Column(TIMESTAMP, nullable=False, index=True, comment="更新时间")

    blogs = db.relationship('BlogModel', backref='user', lazy=True)  # 名下的博客列表
    blog_likes = db.relationship('BlogLikesModel', backref='user', lazy=True)  # 名下的收藏列表
    blog_scores = db.relationship('BlogScoreModel', backref='user', lazy=True)  # 名下的评分列表
    comments = db.relationship('CommentModel', backref='user', lazy=True)  # 名下的评论列表
    people_likes = db.relationship('FansModel', backref='user', lazy=True)  # 喜欢的人

    def __init__(self, uid=None, username=None, password=None, mobile=None, nickname=None, avatar=None, status=None,
                 access=None):
        self.uid = uid
        self.username = username
        self.password = password
        self.mobile = mobile
        self.nickname = nickname
        self.avatar = avatar
        self.status = status
        self.access = access

    def add_update(self):
        pass

    def getUser(self):
        uid = session.get('uid')
        user = UserModel.query.filter_by(uid=uid, status=1).first()
        if user:
            user.access = json.loads(user.access)
        db.session.close()
        return user

    def userList(self, page=1, limit=10):
        offset = (page - 1) * limit
        ulist = UserModel.query.order_by(UserModel.uid.desc()).offset(offset).limit(limit).all()
        for i in range(len(ulist)):
            ulist[i] = ulist[i].to_dict()
            del ulist[i]['password']
            ulist[i]['access'] = json.loads(ulist[i]['access'])
            ulist[i]['access_alert'] = False
            ulist[i]['create_time'] = str(ulist[i]['create_time'])
            ulist[i]['update_time'] = str(ulist[i]['update_time'])
        return ulist

    # 把SQLAlchemy查询对象转换成字典
    def to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return '<TestUser %r>' % self.username
