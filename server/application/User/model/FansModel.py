from application import db
from sqlalchemy.sql import func
from sqlalchemy import TIMESTAMP


class FansModel(db.Model):
    __tablename__ = "fans"

    id = db.Column(db.SmallInteger, primary_key=True, autoincrement=True, nullable=False, index=True, comment='id')
    from_uid = db.Column(db.SmallInteger, db.ForeignKey('user.uid'), nullable=False, index=True, comment='谁发起的喜欢')
    to_uid = db.Column(db.SmallInteger, nullable=False, index=True, comment='被喜欢的用户')
    type = db.Column(db.SmallInteger, nullable=False, index=True, comment='0收回表态/1喜欢/2不喜欢')
    create_time = db.Column(db.DateTime, nullable=False, server_default=func.now(), index=True, comment="创建时间")
    update_time = db.Column(TIMESTAMP, nullable=False, index=True, comment="更新时间")

    def __init__(self, from_uid=None, to_uid=None, type=None):
        self.from_uid = from_uid
        self.to_uid = to_uid
        self.type = type

    def __repr__(self):
        return '<FansModel {}>'.format(self.id)
