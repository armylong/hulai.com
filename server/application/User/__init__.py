from flask import Blueprint

user = Blueprint('user', __name__)

from .controller import UserController
from .controller import ModifyUserController
