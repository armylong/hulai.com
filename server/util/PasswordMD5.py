import hashlib


class PasswordMD5(object):
    salt_public = "armylong"  # 公盐

    def password_MD5(self, password, salt_public=salt_public, salt_private=None):
        password_encode = hashlib.md5(salt_public.encode('utf-8'))  # 加公盐
        if salt_private:  # 如果有私盐
            password_encode.update(salt_private.encode('utf-8'))  # 就加私盐
        password_encode.update(password.encode('utf-8'))  # 密码加密

        return password_encode.hexdigest()  # 返回16进制


if __name__ == '__main__':
    username = "armylong"
    password = "12345679"

    res = PasswordMD5().password_MD5(password=password, salt_private=username)

    print(res)
