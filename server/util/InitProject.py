import os
import sys


class InitProject:
    PROJECT_NAME = "server"

    def __init__(self):
        self.setProjectEnv()

    def setProjectEnv(self):
        sys.path.append(self.getProjectPath())
        return True

    def getProjectPath(self):
        root_path = os.path.abspath(__file__)
        root_path_list = root_path.split('/')
        for path_index in range(len(root_path_list)):
            if root_path_list[path_index] == self.PROJECT_NAME:
                root_path_list = root_path_list[:path_index + 1]
                break
        root_path = '/'.join(root_path_list)
        print("project path: ", root_path)
        return root_path


# RUN
InitProject()
