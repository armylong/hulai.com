from PIL import Image
import os


class CompressImage(object):

    # file:文件, outfile:输出目录, toFileKb:目标文件大小
    def __init__(self, infile, outfile=None):

        self.infile = infile
        # 如果没有输出路径,就把输入路径原路输出
        if outfile:
            self.outfile = outfile
        else:
            self.outfile = infile

        self.infileSize = self.getSize(file=infile)
        self.infilePx = self.getPx()

    def __repr__(self):
        return "{} KB".format(self.infileSize)

    def getSize(self, file=''):
        if not file:
            file = self.infile
        b = os.path.getsize(file)
        return b / 1024

    def getPx(self, file=''):
        if not file:
            file = self.infile
        img = Image.open(file)
        x, y = img.size
        return x, y

    # 压缩图片,实测会失真
    def compressImg(self, toKB=100, step=10, quality=80):
        """不改变图片尺寸压缩到指定大小
            :param infile: 压缩源文件
            :param outfile: 压缩文件保存地址
            :param mb: 压缩目标，KB
            :param step: 每次调整的压缩比率
            :param quality: 初始压缩比率
            :return: 压缩文件地址，压缩文件大小
        """
        infile = self.infile
        outfile = self.outfile

        # 如果文件大小 小于等于 目标大小,直接返回True表示完成
        if self.infileSize <= toKB:
            return True
        self.outfileSize = self.infileSize
        img = Image.open(infile)
        # 如果文件大于目标大小,那么进入循环不停压缩直至完成
        while self.outfileSize > toKB:
            # 初始压缩百分之80
            img.save(outfile, quality=quality)
            img.close()
            if quality - step <= 0:
                break
            quality -= step
            img = Image.open(outfile)
            self.outfileSize = self.getSize(outfile)
        img.close()
        return True

    # 按给出的x,y裁切,或者给出任一尺寸,另一尺寸会按比例裁切
    def resizeImg(self, xSize=None, ySize=None):
        """修改图片尺寸
            :param infile: 图片源文件
            :param outfile: 重设尺寸文件保存地址
            :param xSize: 设置的宽度
            :return:
            """
        infile = self.infile
        outfile = self.outfile

        # 如果没有设置尺寸,就直接返回完成
        if not xSize and not ySize:
            self.outfilePx = self.getPx(outfile)
            self.outfileSize = self.getSize(file=outfile)
            return True

        # 根据路径获取img文件对象
        img = Image.open(infile)

        # 如果给了x和y,那么就直接裁切
        if xSize and ySize:
            outImg = img.resize((xSize, ySize), Image.ANTIALIAS)
            outImg.save(outfile)
            self.outfilePx = self.getPx(outfile)
            self.outfileSize = self.getSize(file=outfile)
            return True

        # 获取图片原尺寸
        x, y = img.size

        # 如果只给了x,那么按x的裁切比例裁切y
        if xSize and not ySize:
            ySize = y * (xSize / x)
            if ySize < 1:
                ySize = 1
            else:
                ySize = int(ySize)
            outImg = img.resize((xSize, ySize), Image.ANTIALIAS)
            outImg.save(outfile)
            self.outfilePx = self.getPx(outfile)
            self.outfileSize = self.getSize(file=outfile)
            return True

        # 如果只给了y,那么按y的裁切比例裁切x
        if ySize and not xSize:
            xSize = x * (ySize / y)
            if xSize < 1:
                xSize = 1
            else:
                xSize = int(xSize)
            outImg = img.resize((xSize, ySize), Image.ANTIALIAS)
            outImg.save(outfile)
            self.outfilePx = self.getPx(outfile)
            self.outfileSize = self.getSize(file=outfile)
            return True

    # 取x,y中最小的一个值与目标px匹配,另一个值自动裁切,可以防止头像处留白
    def autoresizeImg(self, toPx=238):
        infile = self.infile
        outfile = self.outfile
        # 根据路径获取img文件对象
        img = Image.open(infile)
        # 获取图片原尺寸
        x, y = img.size
        # 获取较小的一个值
        lowPx = min(x, y)
        if x == lowPx:
            xSize = toPx
            ySize = int(y * (xSize / x))
            outImg = img.resize((xSize, ySize), Image.ANTIALIAS)
            outImg.save(outfile)
            return True
        if y == lowPx:
            ySize = toPx
            xSize = int(x * (ySize / y))
            outImg = img.resize((xSize, ySize), Image.ANTIALIAS)
            outImg.save(outfile)
            return True


if __name__ == '__main__':
    import os

    base_path = "D:\\img_resize\\"
    path_in = base_path + "img\\"

    files = os.listdir(path_in)
    print(files)

    path_out_en = base_path + "img_out_en\\"
    path_out_cn = base_path + "img_out_cn\\"

    for i in range(len(files)):
        print(files[i])
        type = files[i].split(".")[1]
        print(type)
        file_path = path_in + files[i]

        # en
        file_out = path_out_en + "doctor_" + str(i) + "_" + files[i]
        com = CompressImage(file_path, file_out)
        com.autoresizeImg(150)

        # cn
        file_out1 = path_out_cn + "doctor_" + str(i) + "." + type
        com1 = CompressImage(file_path, file_out1)
        com1.autoresizeImg(150)
