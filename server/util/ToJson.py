from flask import jsonify


class ToJson(object):

    @staticmethod
    def _response(code, msg, data):
        if not data:
            if isinstance(data, list):
                data = []
            else:
                data = {}
        return jsonify({'code': code, 'msg': msg, 'data': data})

    @staticmethod
    def code200(data=None, msg='success'):
        return ToJson._response(code=200, msg=msg, data=data)

    @staticmethod
    def code500(msg='error', data=None):
        return ToJson._response(code=500, msg=msg, data=data)

    @staticmethod
    def code302(msg='success', data=None):
        return ToJson._response(code=302, msg=msg, data=data)
