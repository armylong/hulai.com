import pandas as pd

from datetime import datetime, timedelta

def process_work_time(start_time, end_time):
    if start_time == '未打卡' or end_time == '未打卡':
        return 0
    else:
        start_datetime = datetime.strptime(start_time, "%H:%M")
        end_datetime = datetime.strptime(end_time, "%H:%M")
        time_difference = end_datetime - start_datetime

        # 判断上班开始时间并进行相应的工时计算
        if start_datetime < datetime.strptime("11:00", "%H:%M"):
            if end_datetime > datetime.strptime("11:00", "%H:%M"):
                time_difference -= timedelta(minutes=30)
        elif datetime.strptime("11:00", "%H:%M") <= start_datetime <= datetime.strptime("13:00", "%H:%M"):
            if end_datetime > datetime.strptime("13:00", "%H:%M"):
                time_difference -= timedelta(hours=1)

        return time_difference.total_seconds() / 3600
def update_work_time(work_time_list):
    for i in range(len(work_time_list)):
        if work_time_list[i][0] == '未打卡':
            continue
        elif 0 < int(work_time_list[i][0].split(':')[1]) < 30:
            work_time_list[i][0] = work_time_list[i][0].replace(work_time_list[i][0][3:5], '30')
        elif 30 < int(work_time_list[i][0].split(':')[1]) < 60:
            work_time_list[i][0] = work_time_list[i][0].replace(work_time_list[i][0][3:5], '00')
            start_time = datetime.strptime(work_time_list[i][0], "%H:%M")
            end_time = (start_time + timedelta(hours=1)).strftime("%H:%M")
            work_time_list[i][0] = work_time_list[i][0].replace(work_time_list[i][0][0:5], end_time)

        if work_time_list[i][1] == '未打卡':
            continue
        elif 0 < int(work_time_list[i][1].split(':')[1]) < 30:
            work_time_list[i][1] = work_time_list[i][1].replace(work_time_list[i][1][3:5], '00')
        elif 30 < int(work_time_list[i][1].split(':')[1]) < 60:
            work_time_list[i][1] = work_time_list[i][1].replace(work_time_list[i][1][3:5], '30')

def calculate_total_work_time(work_time_list):
    time_differences = []
    for time_range in work_time_list:
        start_time = time_range[0]
        end_time = time_range[1]
        time_difference = process_work_time(start_time, end_time)
        time_differences.append(time_difference)
    total_work_time = sum(time_differences)
    return total_work_time

if __name__ == '__main__':
    try:
        df = pd.read_excel(r'C:\Users\fll\Desktop\工作打卡时间\上下班打卡_日报_20230901-20230930.xlsx', sheet_name='概况统计与打卡明细')
        data_WorkTime = df.iloc[3:, [7, 8]]
        work_time_list = data_WorkTime.to_numpy().tolist()
        update_work_time(work_time_list)
        total_work_time = calculate_total_work_time(work_time_list)
        print("工时的总和为 {} 小时".format(total_work_time))
    except Exception as e:
        print("发生错误：", str(e))